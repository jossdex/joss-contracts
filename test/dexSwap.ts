import fixtures from "./fixtures";
import { ethers } from "hardhat";
import { expect } from "chai";
import { Contract } from "@ethersproject/contracts";
import { BigNumber, ContractFactory } from "ethers";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";

describe("Router", async function () {
  let ownerWallet: SignerWithAddress;
  let feeWallet: SignerWithAddress;
  let Factory: Contract;
  let deployedFactory: Contract;
  let WAVAX: Contract;
  let wavax: Contract;
  let Router: Contract;
  let router: Contract;

  before(async () => {
    [ownerWallet, feeWallet] = await ethers.getSigners();
    WAVAX = await ethers.getContractAt("IWAVAX", fixtures.Tokens.WAVAX);
    Factory = await ethers.getContractAt("IUniswapV2Factory", fixtures.Factory);
    Router = await ethers.getContractAt("IRouter", fixtures.Router);
  });

  it("should be true", function () {
    expect("").to.equal(true);
  });

  describe("Administration", async function () {
    it("Can be deployed", async function () {
      expect(await router.connect(ownerWallet)).to.be.true;
    });
  });
});
